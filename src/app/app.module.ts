import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { QuillModule } from 'ngx-quill';
import { HttpClientModule } from '@angular/common/http';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    QuillModule.forRoot(),
    CKEditorModule
  ],
  providers: [
    { provide: 'API_ROOT_URL', useValue: environment.api.baseUrl }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
