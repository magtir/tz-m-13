export interface CityModelInterface {
  Id: string;
  HashTag: string;
  Title: string;
}
