import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { CityModelInterface } from '@common/interfaces/city-model.interface';

@Injectable({
  providedIn: 'root'
})
export class CitiesApiService {

  constructor(private api: ApiService) {
  }

  getCities(): Observable<CityModelInterface[]> {
    return this.api.get({
      url: 'lexstark/ngx-test/tags'
    });
  }
}
