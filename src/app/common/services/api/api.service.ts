import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParameterCodec, HttpParams } from '@angular/common/http';
import { cleanObject } from '@common/utils/common.utils';

interface SendQueryParams {
  type: 'get' | 'post' | 'put' | 'patch' | 'delete';
  url: string;
  body?: any;
  params?: any;
  headers?: any;
  pushHeaders?: any;
  options?: any;
}

interface GetParams {
  url: string;
  params?: any;
  headers?: any;
  pushHeaders?: any;
  options?: any;
  transferStateKey?: string;
}

interface PostParams {
  url: string;
  body?: any;
  params?: any;
  headers?: any;
  pushHeaders?: any;
  options?: any;
  transferStateKey?: string;
}

const DEFAULT_HEADERS = {
  ['Content-Type']: 'application/json',
  ['Accept']: 'application/json'
};

class CustomEncoder implements HttpParameterCodec {
  encodeKey(key: string): string {
    return encodeURIComponent(key);
  }

  encodeValue(value: string): string {
    return encodeURIComponent(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string): string {
    return decodeURIComponent(value);
  }
}

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private httpClient: HttpClient,
              @Inject('API_ROOT_URL') private apiRootUrl: string) {
  }

  get(params: GetParams): Observable<any> {
    return this.sendQuery({
      type: 'get',
      ...params
    });
  }

  post(params: PostParams): Observable<any> {
    return this.sendQuery({
      type: 'post',
      ...params
    });
  }

  put(params: PostParams): Observable<any> {
    return this.sendQuery({
      type: 'put',
      ...params
    });
  }

  patch(params: PostParams): Observable<any> {
    return this.sendQuery({
      type: 'patch',
      ...params
    });
  }

  delete(params: GetParams): Observable<any> {
    return this.sendQuery({
      type: 'delete',
      ...params
    });
  }

  sendQuery(params: SendQueryParams): Observable<any> {
    let httpHeaders;
    if (!params.headers) {
      httpHeaders = new HttpHeaders({ ...DEFAULT_HEADERS, ...params.pushHeaders });
    } else {
      httpHeaders = new HttpHeaders(params.headers);
    }
    let httpParams = new HttpParams({ encoder: new CustomEncoder() });
    if (params.params) {
      Object.keys(params.params)
        .forEach(param => {
          httpParams = httpParams.append(param, params.params[param]);
        });
    }
    const url = `${ this.apiRootUrl }${ params.url }`;
    const queryParams = cleanObject({
      headers: httpHeaders, params: httpParams, options: params.options
    });
    let query;
    if (params.type === 'get' || params.type === 'delete') {
      query = this.httpClient[params.type](url, queryParams);
    } else {
      query = this.httpClient[params.type](url, params.body, queryParams);
    }

    return query;
  }
}
