export function isExists(value, withEmptyString?: boolean): boolean {
  return value !== null && value !== undefined && (!withEmptyString || value !== '');
}

export function cleanObject<T = object>(obj: object, withEmptyString?: boolean): T {
  const cleanedObject = {};

  Object
    .keys(obj)
    .forEach(key => {
      if (isExists(obj[key], withEmptyString)) {
        cleanedObject[key] = obj[key];
      }
    });

  return cleanedObject as T;
}

export function copyTextToClipboard(value: string, elemAfterFocus?, wrapper?): void {
  if (!wrapper) {
    wrapper = document.body;
  }
  const selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '-9999';
  selBox.style.top = '-9999';
  selBox.style.opacity = '0';
  selBox.value = value;
  wrapper.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  wrapper.removeChild(selBox);

  if (elemAfterFocus) {
    elemAfterFocus.focus();
  }
}
