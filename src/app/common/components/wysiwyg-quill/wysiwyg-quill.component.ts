import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Subject } from 'rxjs';
import { CityModelInterface } from '@common/interfaces/city-model.interface';
import { customBots } from '@common/components/wysiwyg-quill/custom-blots';
import { copyTextToClipboard } from '@common/utils/common.utils';
import { AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'app-wysiwyg-quill',
  templateUrl: './wysiwyg-quill.component.html',
  styleUrls: ['./wysiwyg-quill.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WysiwygQuillComponent implements OnInit, OnDestroy {
  componentDestroyed$ = new Subject();

  listeners = [];

  @Input() control: AbstractControl | FormControl;
  @Input() quillOptions: { modules?: any, placeholder?: string };
  @Input() cites: CityModelInterface[] = [];
  citiesMap: any = {};
  text = '';
  quill: any;

  mouseIsDown = false;

  constructor(private renderer: Renderer2) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.text = this.control.value;
    });

    // распределяем массив марков в мапу, чтобы можно было быстрее найти нужный по его ключу
    this.cites.forEach(city => this.citiesMap[city.HashTag] = city);
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
    this.listeners
      .filter(l => l)
      .forEach(l => l());
  }

  initQuillAndListeners(quill): void {
    this.quill = quill;
    this.initListeners();
  }

  initListeners() {
    this.listeners.push(this.renderer.listen(this.quill.container, 'mousedown', () => {
      // во время нажатия мыши
      this.mouseIsDown = true;
      // делаем марки нередактирвуемые
      this.setLockAllMarksCity(true);
    }));

    this.listeners.push(this.renderer.listen(document, 'mouseup', () => {
      // делаем марки редактирвуемые, чтобы можно было их выделять
      this.setLockAllMarksCity(false);
      this.mouseIsDown = false;
    }));

    this.listeners.push(this.renderer.listen(this.quill.container, 'copy', () => this.copyTextWithReplaceCitiesToHashtags()));
    this.listeners.push(this.renderer.listen(this.quill.container, 'cut', () => this.copyTextWithReplaceCitiesToHashtags(true)));
    this.listeners.push(this.renderer.listen(this.quill.container, 'paste', () => {
      setTimeout(() => this.replaceHashtagsToCitiesByString(), 100 /*для тупого фаерфокса*/);
    }));
    this.listeners.push((this.renderer.listen(this.quill.container, 'keydown', (e: KeyboardEvent) => {
      if (this.mouseIsDown) {
        // так как во время зажатой конпки мыши все марки становятся редактируемыми
        // то если она зажата, останавливаем все клавиши, чтобы их не изменить
        e.preventDefault();
      } else {
        // ждем пока нажмут пробел или ентер, что бы проверить напечатаные хештеги
        if (e.code === 'Space' || e.code === 'Enter') {
          setTimeout(() => this.replaceHashtagsToCitiesByString());
        }
      }
    })));
  }

  onContentChanged({ text, html }): void {
    if (text) {
      this.control.setValue(html);
    }
  }

  copyTextWithReplaceCitiesToHashtags(isCut?: boolean) {
    // запоминаем текущее выделение
    const selectionQuill = this.quill.getSelection();

    // получаем выделенный текст
    const selectionWindow = window.getSelection();
    const range = selectionWindow.getRangeAt(0);
    const documentFragment = range.cloneContents();
    const allCities = documentFragment.querySelectorAll('[data-city]');
    Object.keys(allCities).forEach(key => {
      const elem = allCities[key];
      const hashtag = elem.getAttribute('data-hashtag');
      const currentTitle = elem.innerText;
      if (this.citiesMap[hashtag]) {
        if (this.citiesMap[hashtag].Title === currentTitle) {
          this.replaceElemMarkCityToElemHashtag(elem, hashtag);

          // если город нашелся, но он выбран не полностью и это вырезание
        } else if (isCut) {
          // смотрим, какая часть не выделена
          const arr = this.citiesMap[hashtag].Title.split(currentTitle);
          // если первый пустой, то значит с него было выделение.
          if (!arr[0]) {
            // сдвигаем индекс на его начало, что бы его удалить тоже
            selectionQuill.index--;
          }

          // если второй пустой, то значит на нем заканчивается выделение.
          if (!arr[1]) {
            // дозахватываем последний
            selectionQuill.length++;
          }

          // при этом всё равно заменяем строчку на хештег
          this.replaceElemMarkCityToElemHashtag(elem, hashtag);
        }
      }
    });
    const tempContainer = document.createElement('div');
    tempContainer.appendChild(documentFragment);
    const copiedText = tempContainer.innerText;

    if (isCut) {
      // если вырезаем, то сдвигаем выбранную область так, что бы зацепить города, которые не полностью попали
      this.quill.setSelection(selectionQuill.index, selectionQuill.length);
    }

    // после копирования возвращаем выделение
    setTimeout(() => {
      // уводим в буфер обмена изменную строку
      copyTextToClipboard(copiedText);

      if (isCut) {
        // если вырезали, то ставим в начало
        this.quill.setSelection(selectionQuill.index);
      } else {
        // если копировали, то в конец
        this.quill.setSelection(selectionQuill.index + selectionQuill.length);
      }
    });
  }

  replaceElemMarkCityToElemHashtag(elem, hashtag): void {
    const newElem = document.createElement('span');
    newElem.innerText = hashtag;
    elem.parentNode.replaceChild(newElem, elem);
  }

  replaceHashtagsToCitiesByString() {
    // проверяем есть ли там символ #
    const str = this.customGetText();
    if (str && str.includes && str.includes('#')) {
      const strArr = str.split(/[\s\n#]/g);
      strArr.forEach(itemAsHashtag => {
        itemAsHashtag = `#${ itemAsHashtag }`;
        if (this.citiesMap[itemAsHashtag]) {
          // если находим такой марк по key
          // определяем индекс, куда надо его вставить
          const indexStartCut = this.customGetText().indexOf(itemAsHashtag);
          // удаляем название хештега
          this.quill.deleteText(indexStartCut, itemAsHashtag.length);
          // добавляем марк
          this.addMarkCity(this.citiesMap[itemAsHashtag], false, false, indexStartCut);
        }
      });
    }
  }

  onBlur(): void {
    this.control.markAsTouched();
  }

  addMarkCity(city: CityModelInterface, withAfterSelection: boolean, withAfterSpace: boolean, indexStart?: number): void {
    // если индекс не передан, то берем текущую позицию каретки
    if (!indexStart && indexStart !== 0) {
      indexStart = this.quill.getSelection(true).index;
    }
    let indexEnd = indexStart + 1;
    if (withAfterSpace) {
      this.quill.insertText(indexStart, ' ');
      indexEnd++;
    }
    this.quill.insertEmbed(indexStart, customBots.markCity.blotName, city);
    if (withAfterSelection) {
      this.quill.setSelection(indexEnd);
    }
  }

  // поменять возможность редактирования
  setLockAllMarksCity(flag: boolean): void {
    const cities = this.quill.container.querySelectorAll(`.${ customBots.markCity.className }`);
    if (cities) {
      cities.forEach((cityElem: HTMLElement) => this.setLockMarkCity(cityElem, flag));
    }
  }

  setLockMarkCity(cityElem: HTMLElement, flag: boolean): void {
    if (cityElem.firstElementChild) {
      cityElem.firstElementChild.setAttribute('contenteditable', flag ? 'true' : 'false');
    }
  }

  customGetText() {
    return this.quill.getContents()
      .filter(op => typeof op.insert === 'string' || op.insert[customBots.markCity.blotName]).map(op => {
        if (op.insert[customBots.markCity.blotName]) {
          return 'm';
        }
        return op.insert;
      }).join('');
  }
}
