import * as QuillNamespace from 'quill';
import { CityModelInterface } from '@common/interfaces/city-model.interface';

const Quill: any = QuillNamespace;
const BlotEmbed = Quill.import('blots/embed');

export const customBots = {
  markCity: { blotName: 'mark-city', className: 'mark-city', tagName: 'span' }
};

////////////////////////////////

class MarkLink extends BlotEmbed {
  static create(mark: CityModelInterface) {
    let node;
    node = super.create();
    node.setAttribute('data-hashtag', mark.HashTag);
    node.setAttribute('data-title', mark.Title);
    node.innerHTML = `<span data-city data-hashtag="${ mark.HashTag }">${ mark.Title }</span>`;
    return node;
  }

  static value = (node) => ({
    HashTag: node.getAttribute('data-hashtag'),
    Title: node.getAttribute('data-title'),
  })
}

MarkLink.blotName = customBots.markCity.blotName;
MarkLink.className = customBots.markCity.className;
MarkLink.tagName = customBots.markCity.tagName;
Quill.register(MarkLink);
