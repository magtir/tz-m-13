import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WysiwygQuillComponent } from './wysiwyg-quill.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    QuillModule
  ],
  declarations: [WysiwygQuillComponent],
  exports: [WysiwygQuillComponent]
})

export class WysiwygQuillModule {
}
