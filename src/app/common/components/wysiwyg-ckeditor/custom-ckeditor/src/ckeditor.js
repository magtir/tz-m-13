/**
 * @license Copyright (c) 2014-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';

import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Command from '@ckeditor/ckeditor5-core/src/command';

import Undo from '@ckeditor/ckeditor5-undo/src/undo';

import { viewToModelPositionOutsideModelElement } from '@ckeditor/ckeditor5-widget/src/utils';

let allCitiesElem;
let allCitiesMap = {};

export default class Editor extends ClassicEditor {
}

class Init extends Plugin {
  init() {
    allCitiesElem = document.querySelectorAll('.city-btn');
    Object.keys(allCitiesElem).forEach(key => {
      const elem = allCitiesElem[key];
      const title = elem.getAttribute('data-title');
      const hashtag = elem.getAttribute('data-hashtag');
      allCitiesMap[hashtag] = title;
    });

    this.editor.editing.view.document.on('keydown', (evt, data) => {
      if (data.domEvent.key === 'Enter' || data.domEvent.key === ' ') {
        replaceHashtagsToCitiesByString(this.editor, data.domEvent.key, evt, data);
      }
    });

    document.onclick = e => {
      if (e.target.classList.contains('city-btn')) {
        const title = e.target.getAttribute('data-title');
        const hashtag = e.target.getAttribute('data-hashtag');
        this.editor.execute('mark', { title, hashtag });
        this.editor.editing.view.focus();
      }
    }

    this.editor.editing.view.document.on('copy', () => {
      saveInClipboard();
    });

    this.editor.editing.view.document.on('cut', () => {
      saveInClipboard();
    });

    this.editor.editing.view.document.on('clipboardInput', (evt, data) => {
      const dataTransfer = data.dataTransfer;
      let content = dataTransfer.getData('text/plain');
      content = content
        .split(/\s|\n|&nbsp;/g)
        .map(itemAsHashtag => {
          if (allCitiesMap[itemAsHashtag]) {
            return `<span class="mark-city" data-title="${allCitiesMap[itemAsHashtag]}" data-hashtag="${itemAsHashtag}">${allCitiesMap[itemAsHashtag]}</span>`;
          } else {
            return itemAsHashtag;
          }
        })
        .join(' ');
      const viewFragment = this.editor.data.processor.toView(content);
      const modelFragment = this.editor.data.toModel(viewFragment);
      this.editor.model.insertContent(modelFragment);
      evt.stop();
    });

    // Expose for playing in the console.
    window.editor = this.editor;
  }
}

class MarkCommand extends Command {
  execute({ title, hashtag }) {
    this.editor.model.change(writer => {
      const mark = writer.createElement('mark', { ['data-title']: title, ['data-hashtag']: hashtag });
      this.editor.model.insertContent(mark);
    });
  }
}

class Mark extends Plugin {
  init() {
    this._defineSchema();
    this._defineConverters();

    this.editor.commands.add('mark', new MarkCommand(this.editor));

    this.editor.editing.mapper.on(
      'viewToModelPosition',
      viewToModelPositionOutsideModelElement(this.editor.model, viewElement => viewElement.hasClass('mark-city'))
    );
  }

  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.register('mark', {
      allowWhere: '$text',
      isInline: true,
      isObject: true,
      isSelectable: true,
      allowAttributes: ['data-title', 'data-hashtag']
    });
  }

  _defineConverters() {
    const conversion = this.editor.conversion;

    conversion.for('upcast').elementToElement({
      view: {
        name: 'span',
        classes: ['mark-city']
      },
      model: (viewElement, { writer: modelWriter }) => {
        return modelWriter.createElement('mark', {
          ['data-title']: viewElement.getAttribute('data-title'),
          ['data-hashtag']: viewElement.getAttribute('data-hashtag')
        });
      }
    });

    conversion.for('downcast').elementToElement({
      model: 'mark',
      view: (modelItem, { writer: viewWriter }) => {
        return createMarkView(modelItem, viewWriter)
      }
    });

    conversion.for('dataDowncast').elementToElement({
      model: 'mark',
      view: (modelItem, { writer: viewWriter }) => {
        return createMarkView(modelItem, viewWriter)
      }
    });

    function createMarkView(modelItem, viewWriter) {
      const title = modelItem.getAttribute('data-title');
      const hashtag = modelItem.getAttribute('data-hashtag');

      const markView = viewWriter.createContainerElement('span', {
        class: 'mark-city',
        ['data-title']: title,
        ['data-hashtag']: hashtag
      });

      const innerText = viewWriter.createText(title);
      viewWriter.insert(viewWriter.createPositionAt(markView, 0), innerText);

      return markView;
    }
  }
}

Editor.builtinPlugins = [
  Init, Essentials, Paragraph, Mark, Undo
];

function saveInClipboard() {
  const selectionWindow = window.getSelection();
  const range = selectionWindow.getRangeAt(0);
  const documentFragment = range.cloneContents();
  const allCities = documentFragment.querySelectorAll('.mark-city');
  Object.keys(allCities).forEach(key => {
    const elem = allCities[key];
    const hashtag = elem.getAttribute('data-hashtag');
    const newElem = document.createElement('span');
    newElem.classList.add('mark-cut');
    newElem.innerText = hashtag;
    elem.parentNode.replaceChild(newElem, elem);
  });
  const tempContainer = document.createElement('div');
  tempContainer.appendChild(documentFragment);
  const copiedHTML = tempContainer.innerText;
  setTimeout(() => {
    copyTextToClipboard(copiedHTML);
  });
}

function replaceHashtagsToCitiesByString(editor, keyboardKey, evt, data) {
  // проверяем есть ли там символ #
  let str = editor.getData();
  if (str && str.includes && str.includes('#')) {
    const strArr = str.split(/\s|\n|<|>|&nbsp;/g);
    strArr.forEach(itemAsHashtag => {
      if (allCitiesMap[itemAsHashtag]) {
        data.preventDefault();
        evt.stop();

        if (keyboardKey === 'Enter') {
          editor.execute('undo');
        }

        for (let i = 0; i < itemAsHashtag.length; i++) {
          document.execCommand('delete');
        }
        setTimeout(() => {
          editor.execute('mark', { title: allCitiesMap[itemAsHashtag], hashtag: itemAsHashtag });
        });
      }
    });
  }
}

function copyTextToClipboard(value, elemAfterFocus, wrapper) {
  if (!wrapper) {
    wrapper = document.body;
  }
  const selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '-9999';
  selBox.style.top = '-9999';
  selBox.style.opacity = '0';
  selBox.value = value;
  wrapper.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  wrapper.removeChild(selBox);

  if (elemAfterFocus) {
    elemAfterFocus.focus();
  }
}
