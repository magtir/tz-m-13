import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WysiwygCkeditorComponent } from '@common/components/wysiwyg-ckeditor/wysiwyg-ckeditor.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CKEditorModule
  ],
  declarations: [WysiwygCkeditorComponent],
  exports: [WysiwygCkeditorComponent]
})

export class WysiwygCkeditorModule {
}
