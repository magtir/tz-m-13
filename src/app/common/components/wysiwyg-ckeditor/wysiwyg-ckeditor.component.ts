import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { CityModelInterface } from '@common/interfaces/city-model.interface';
import { AbstractControl, FormControl } from '@angular/forms';
import * as Editor from './custom-ckeditor/build/ckeditor';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular';

@Component({
  selector: 'app-wysiwyg-ckeditor',
  templateUrl: './wysiwyg-ckeditor.component.html',
  styleUrls: ['./wysiwyg-ckeditor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WysiwygCkeditorComponent implements OnInit {
  @Input() control: AbstractControl | FormControl;
  @Input() cites: CityModelInterface[] = [];
  @Input() ckeditorOptions: CKEditor5.Config;

  Editor = Editor;

  text = '';

  constructor() {
  }

  ngOnInit(): void {
    this.text = this.control.value;
  }

  onReady(editor): void {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  change(html): void {
    this.control.setValue(html);
  }
}
