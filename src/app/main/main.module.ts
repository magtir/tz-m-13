import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainLayoutModule } from './main-layout/main-layout.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MainLayoutModule,
    MainRoutingModule
  ]
})
export class MainModule {
}
