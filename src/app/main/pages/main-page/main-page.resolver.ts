import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, first } from 'rxjs/operators';
import { CitiesApiService } from '@common/services/api/cities-api.service';

@Injectable()
export class MainPageResolver implements Resolve<any> {
  constructor(private citiesApiService: CitiesApiService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.citiesApiService.getCities()
      .pipe(first(), catchError(() => of(null)));
  }
}
