import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CityModelInterface } from '@common/interfaces/city-model.interface';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';

const testFieldData = '<p><span class="mark-city" data-hashtag="#msk" data-title="Москва"><span><span data-city="" data-hashtag="#msk">Москва</span></span></span> 123 3<span class="mark-city" data-hashtag="#spb" data-title="Санкт-Петербург"><span contenteditable="false"><span data-city="" data-hashtag="#spb">Санкт-Петербург</span></span></span></p>';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainPageComponent implements OnInit {
  control = new FormControl(testFieldData);

  quillOptions: any = {
    modules: {
      toolbar: false
    },
    placeholder: 'Input text'
  };

  ckeditorOptions: CKEditor5.Config = {
    placeholder: 'Input text'
  };

  cites: CityModelInterface[] = [];

  isError$ = new BehaviorSubject(false);

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    const pageData = this.activatedRoute.snapshot.data.pageData;
    if (pageData) {
      this.cites = pageData;
    } else {
      this.isError$.next(true);
    }
  }
}
