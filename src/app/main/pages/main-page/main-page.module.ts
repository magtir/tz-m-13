import { NgModule } from '@angular/core';
import { WysiwygQuillModule } from '@common/components/wysiwyg-quill/wysiwyg-quill.module';
import { MainPageResolver } from './main-page.resolver';
import { MainPageComponent } from './main-page.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { WysiwygCkeditorModule } from '@common/components/wysiwyg-ckeditor/wysiwyg-ckeditor.module';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
    resolve: { pageData: MainPageResolver }
  }
];

@NgModule({
  declarations: [
    MainPageComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    WysiwygQuillModule,
    CommonModule,
    WysiwygCkeditorModule
  ],
  providers: [
    MainPageResolver
  ],
})
export class MainPageModule {
}
